import axios from "axios";

let token = ''

export const getToken = () => {
    return token
}

// holoyis165@bulkbye.com
// placki777


export const login = () => {
    var client_id = '16d3922d13184d1c9f5fdb6c46135f9e';
    var redirect_uri = window.location.origin + '/' //'http://localhost:8080/';

    // var state = generateRandomString(16);
    // localStorage.setItem(stateKey, state);

    var scope = 'user-read-private user-read-email';

    var url = 'https://accounts.spotify.com/authorize';
    url += '?response_type=token';
    url += '&client_id=' + encodeURIComponent(client_id);
    url += '&scope=' + encodeURIComponent(scope);
    url += '&redirect_uri=' + encodeURIComponent(redirect_uri);
    // url += '&state=' + encodeURIComponent(state);

    window.location.href = url;
}

export const authInit = () => {

    if (window.location.hash) {
        token = new URLSearchParams(window.location.hash).get('#access_token')
    }

    if (token) {
        sessionStorage.setItem('token', token)
        window.location.hash = ''
    } else {
        token = sessionStorage.getItem('token')
    }

    if (!token) {
        login()
    }
}

export const logout = () => {
    sessionStorage.removeItem('token')
    token = ''
    window.location.href = window.location.origin
}

axios.defaults.baseURL = 'https://api.spotify.com/v1/'

axios.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        Authorization: 'Bearer ' + getToken()
    }

    return config
})

axios.interceptors.response.use(ok => ok, error => {
    console.error(error);

    if (!axios.isAxiosError(error)) {
        throw new Error('Invalid server reponse')
    }

    if (error.response.status === 401) {
        login()
    }

    throw new Error(error.response.data.error.message)

    // Update token and retry request
    // const config = {
    //     ...error.config,
    //     headers: {
    //         ...config.headers,
    //         Authorization: 'Bearer ' + getToken()
    //     }
    // }
    // return axios.request(config)
})