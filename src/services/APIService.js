import axios from "axios"

// JSDOC 

/**
 * @typedef Artist
 * @property {string} name
 */


/**
 * Fetch albums from server
 * @param {string} query 
 * @returns {Promise<Album[]>}
 */
export const fetchAlbums = async (query) => {
    const res = await fetch("http://localhost:8080/albums.json")
    return await res.json()
}

// fetchAlbums('batman').then(res => res[0].tracks.items[0].name)

export const fetchAlbumSearchResults = (query) => {
    return axios.get('search', {
        params: {
            type: 'album',
            query: query
        },
    })
        .then(res => res.data.albums.items)
}

export const fetchAlbumById = (id) => {
    return axios.get('albums/' + id, {})
        .then(res => res.data)
}

export const fetchArtistSearchResults = (query) => {
    return axios.get('search', {
        params: {
            type: 'artist',
            query: query
        },
    })
        .then(res => res.data.artists.items)
}

export const fetchPlaylistById = (id) => {
    return axios.get('playlists/' + id, {})
        .then(res => res.data)
}

export const fetchCurrentUserPlaylists = () => {
    return axios.get('me/playlists', {})
        .then(res => res.data.items)
}

export const fetchCurrentUserProfile = () => {
    return axios.get('me', {})
        .then(res => res.data)
}