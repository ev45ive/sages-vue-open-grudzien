import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import PlaylistsViewVuex from "../views/playlists/PlaylistsViewVuex.vue";
import PlaylistsDetailsView from "../views/playlists/PlaylistsDetailsView.vue";
import PlaylistsFormView from "../views/playlists/PlaylistsFormView.vue";
import AlbumsSearchVuex from "../views/search/AlbumsSearchVuex.vue";
import AlbumDetailsView from "../views/AlbumDetailsView.vue";
import CounterView from "../views/CounterView.vue";
import store from "../store";
// import PlaylistList from "../components/playlists/PlaylistList.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/playlists",
    name: "Playlists",
    component: PlaylistsViewVuex,
    children: [
      {
        path: '', component: {
          render(h) {
            return h('p', ['Please select playlist'])
          }
        },
      },
      {
        path: ':playlistId', name: 'PlaylistDetails', component: PlaylistsDetailsView
      },
      { path: 'edit', name: 'PlaylistDetails', component: PlaylistsFormView },
      // { path: 'create', component: PlaylistFormView }
    ]
  },
  {
    path: "/search",
    name: "Search",
    component: AlbumsSearchVuex,
  },
  {
    path: "/albums/:albumId/details",
    name: "Albums",
    component: AlbumDetailsView,
    // beforeEach: (to, from, next) => { }
  },
  {
    path: "/counter",
    name: "Counter",
    component: CounterView,
  },
  {
    path: 'access_token*',
    redirect: '/search'
  },
  {
    path: "/",
    redirect: '/search'
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: '**',
    component: {
      render(h) {
        return h('h1', { staticClass: 'text-center mt-4' }, ['404 Page not found'])
      }
    }
  }
];

const router = new VueRouter({
  mode: "history",
  // mode: "hash",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'active'
});

export default router;

router.beforeEach(async (to, from, next) => {
  next(await store.dispatch('navigation', { to, from }))
})

// router.beforeEach((to,from,next)=>{})
// router.onError