export const uiModule = {
    namespaced: true,
    state: {
        loading: false,
        error: null,
        navbar: {
            open: false
        }
    },
    mutations: {
        'LOADING_START'(state, payload) {
            state.loading = true
            state.error = null
        },
        'LOADING_SUCCESS'(state, payload) {
            state.loading = false
        },
        'LOADING_FAILED'(state, error) {
            state.loading = false
            state.error = error
        },
    },
    actions: {},
    getters: {
        loading: s => s.loading,
        error: s => s.error,
    },
};
