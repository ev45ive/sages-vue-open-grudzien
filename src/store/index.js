import Vue from "vue";
import Vuex from "vuex";
import { counterModule } from "./counterModule";
import { playlistsModule } from "./playlistsModule";
import { searchModule } from "./searchModule";
import { uiModule } from "./uiModule";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // counter <= counterModule
  },
  mutations: {}, actions: {},
  modules: {
    counter: counterModule,
    'search': searchModule,
    'ui': uiModule,
    playlists: playlistsModule
  },
  actions: {
    async navigation({ commit, dispatch }, { to, from }) {
      console.log(to)
      return undefined
    }
  }
});
