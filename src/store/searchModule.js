import { mockAlbums } from "../mocks/mockAlbums";
import { fetchAlbumSearchResults } from "../services/APIService";

export const searchModule = {
    namespaced: true,
    state: {
        query: 'batman',
        results: mockAlbums
    },
    mutations: {
        'SEARCH_START'(state, query) {
            state.results = []
            state.query = query;
        },
        'SEARCH_SUCCESS'(state, result) {
            state.results = result
        },
        'SEARCH_FAILED'(state, error) { },
    },
    // commnads
    actions: {
        async searchAlbums({ commit, state, dispatch }, query) {
            try {
                if (!query) {
                    commit('SEARCH_START', '')
                    return []
                }
                if (query === state.query) {
                    return state.results;
                }
                // commit('LOADING_START', null, { root: true })
                commit('SEARCH_START', query)
                const result = await fetchAlbumSearchResults(state.query)
                commit('SEARCH_SUCCESS', result)
                // commit('LOADING_SUCCESS', null, { root: true })
                return result
            } catch (error) {
                commit('SEARCH_FAILED', error)
                // commit('LOADING_FAILED', error, { root: true })
            }
        }
    },
    // queries
    getters: {
        results: state => state.results,
        query: state => state.query,
    },
};
