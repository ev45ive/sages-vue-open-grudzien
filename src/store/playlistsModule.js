import Vue from "vue";
import { playlistsMock } from "../mocks/playlistsMock";
import { fetchCurrentUserPlaylists, fetchPlaylistById } from "../services/APIService";

export const playlistsModule = {
    namespaced: true,
    state: {
        entities: {
            playlists: {
                123: playlistsMock[0]
            },
            tracks: {},
        },
        lists: {
            myplaylists: [123, 345, 234, 456],
            topplaylists: [123, 345, 234, 456],
        },
        selectedId: 123
    },
    mutations: {
        loadMyPlaylists(state, playlists) {
            state.lists.myplaylists = playlists.map(playlist => {
                // state.entities.playlists[playlist.id] = playlist
                Vue.set(state.entities.playlists, playlist.id, playlist)
                return playlist.id
            })
        },
        selected(state, playlist) {
            if (!playlist) {
                state.selectedId = null
                return
            }
            Vue.set(state.entities.playlists, playlist.id, playlist)
            state.selectedId = playlist.id
        }
    },
    actions: {
        async load({ commit }, payload) {
            const result = await fetchCurrentUserPlaylists()
            commit('loadMyPlaylists', result)
        },
        async select({ commit }, id) {
            const playlist = id && await fetchPlaylistById(id)
            commit('selected', playlist)
        },
        async save({ commit }, draft) {

        },
        async delete({ commit }, payload) {

        },
    },
    getters: {
        myplaylists(state) {
            return state.lists.myplaylists.map(id => state.entities.playlists[id])
        },
        selected(state) {
            return state.entities.playlists[state.selectedId]
        }
    },
};
