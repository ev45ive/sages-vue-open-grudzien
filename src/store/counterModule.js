export const counterModule = {
  namespaced: true,
  state: {
    value: 0,
    message: ''
  },
  mutations: {
    'MESSAGE'(state, payload = '') { state.message = payload; },
    'INC'(state, payload = 1) {
      state.value += payload;
    },
    'DEC'(state, payload = 1) {
      state.value -= payload;
    },
    'RESET'(state, payload = 0) {
      state.value = payload;
    },
  },
  actions: {
    startCounter({ commit, state }, limit = 5) {
      return new Promise(resolve => {
        commit('MESSAGE', 'Counter Started!');
        commit('RESET', 0);
        const handler = setInterval(() => {
          commit('INC', 1);
          if (state.value >= limit) {
            clearInterval(handler);
            commit('MESSAGE', 'Finished!');
            resolve(state.counter.value);
          }
        }, 1000);
      });
    }
  },
  getters: {
    counterMessage(state) {
      return `${state.message} - Counter: ${state.value}`;
    }
  },
};
