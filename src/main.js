import Vue from "vue";
import App, { mojeExtra1 } from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

Vue.filter('titlecase', (val) => {
  return val.substr(0, 1).toUpperCase() + val.substr(1);
})

// Global component - can use in any <template>
import Button from './components/Button.vue'
import Card from './components/Card.vue'
import { authInit } from "./services/AuthService";

Vue.component('Card', Card)
Vue.component('Button', Button)

authInit()


window.app = new Vue({
  router,
  store,
  render: (h) => h(App),
})
  .$mount("#app");
