
export const playlistsMock = [
    {
        id: "123",
        name: "Playlist A123",
        public: true,
        description: "My favourite 123",
    },
    {
        id: "234",
        name: "Playlist B234",
        public: false,
        description: "My favourite 234",
    },
    {
        id: "345",
        name: "Playlist C345",
        public: true,
        description: "My favourite 345",
    },
];
