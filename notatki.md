# GIT

cd ..
git clone https://bitbucket.org/ev45ive/sages-vue-open-grudzien.git sages-vue-open-grudzien
cd sages-vue-open-grudzien
npm i

<!-- File -> OPen folder -> sages-vue-open-grudzien -->
npm run serve

## Update
git stash -u
git pull
<!-- or -->
git pull --set-upstream origin master


# Instalacje 

https://nodejs.org/en/ 
node -v 
v16.13.1

npm -v
6.14.6

https://gitforwindows.org/
git --version
git version 2.31.1.windows.1

https://code.visualstudio.com/
Visual Studio Code
code -v
1.63

Google Chrome	96.0.4664.45 

## Devtools
https://devtools.vuejs.org/guide/installation.html
https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

## Extensions
https://marketplace.visualstudio.com/items?itemName=octref.vetur
https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar
https://marketplace.visualstudio.com/items?itemName=hollowtree.vue-snippets
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

## Vue CLI
npm i -g @vue/cli

vue -V
@vue/cli 4.5.15

vue --help
vue ui

## Create new project
<!-- create in this directory -->
vue create -m npm --merge "."

Vue CLI v4.5.15
? Please pick a preset: Manually select features
 Check the features needed for your project: 
 (*) Choose Vue version
 (*) Babel
 ( ) TypeScript
 ( ) Progressive Web App (PWA) Support        
 (*) Router
 (*) Vuex
 ( ) CSS Pre-processors
 (*) Linter / Formatter
>(*) Unit Testing
 ( ) E2E Testing
? Choose a version of Vue.js that you want to start the project with 2.x
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a linter / formatter config: Prettier
? Pick additional lint features:
? Pick a unit testing solution: Jest
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? Yes
? Save preset as: sages-vue-js
? Pick the package manager to use when installing dependencies: NPM

📄  Generating README.md...

🎉  Successfully created project sages-vue-open-grudzien.
👉  Get started with the following commands:

## Start

 $ npm run serve

 vue-cli-service serve

 INFO  Starting development server...
98% after emitting CopyPlugin

 DONE  Compiled successfully in 5733ms                                                                                                                            11:43:42

  App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.1.107:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.


## Playlists Screen

mkdir -p src/components/playlists/
mkdir -p src/views/playlists/

code src/views/playlists/PlaylistsView.vue
code src/components/playlists/PlaylistList.vue
code src/components/playlists/PlaylistListItem.vue
code src/components/playlists/PlaylistDetails.vue
code src/components/playlists/PlaylistForm.vue

## Vue 3 migration
https://v3.vuejs.org/guide/migration/introduction.html


## Forms 
https://vuejs.org/v2/cookbook/form-validation.html

https://vuelidate.js.org/#sub-installation

https://vueformulate.com/

https://vueform.com/

https://vee-validate.logaretm.com/v3/guide/basics.html#rule-arguments


## Vue UI Kits
https://bootstrap-vue.org/
https://vuetifyjs.com/en/
https://www.antdv.com/docs/vue/introduce/ 

https://athemes.com/collections/vue-ui-component-libraries/

https://vuikit.js.org/guide/modal

https://quasar.dev/vue-components/tabs#usage

https://www.primefaces.org/primevue/showcase/#/

## Lifecycle 
https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram
https://vuejs.org/v2/api/#created



## Album search Screen

mkdir -p src/components/search/
mkdir -p src/components/music/
mkdir -p src/views/search/

code src/views/search/AlbumsSearch.vue
code src/components/search/SearchForm.vue
code src/components/search/SearchResults.vue
code src/components/music/AlbumCard.vue
code src/components/music/ArtistCard.vue

## Renderless components
https://headlessui.dev/vue/menu


## TypeScript
https://vuejs.org/v2/guide/typescript.html
https://class-component.vuejs.org/guide/installation.html#vue-cli-setup
https://github.com/kaorun343/vue-property-decorator


## Vuex
https://martinfowler.com/bliki/CQRS.html
https://vuex.vuejs.org/
https://redux.js.org/understanding/thinking-in-redux/motivation


## REndering XML
https://forum.vuejs.org/t/rendering-xml-with-a-template-system/37638