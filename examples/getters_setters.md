```js
app.message 
'Hello Vue!'
obj = {
    _secret: '',
    get value(){
        return this._secret.split('').reverse().join('')
    },
    set value(val){
        this._secret = val.split('').reverse().join('')
        // + rerender HTML
    },
}
obj.value 
// ''

obj.value = 'Ala ma kota'
// 'Ala ma kota'

obj
// {_secret: 'atok am alA'}

obj.value
// 'Ala ma kota'

```