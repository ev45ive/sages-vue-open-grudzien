[1,2,3,4,5].reduce( (state,x) => {
    return { 
        ...state,
        counter: state.counter + x
    }
}, {
    counter: 0,
    todos: ['test']
})

// ==============================

state = {
    counter: 0,
    todos: ['test']
}


reducer = (state,x) => {
    return { 
        ...state,
        counter: state.counter + x
    }
}

state = reducer(state, 1);
state = reducer(state, 2);
state = reducer(state, 3);
state = reducer(state, 4);
// {counter: 10, todos: Array(1)}
state = reducer(state, 5);
// {counter: 15, todos: Array(1)}


// ==============================

state = {
    counter: 0,
    todos: ['test']
}


reducer = (state,action) => {
    switch(action.type){
        case 'INC': return { 
            ...state, counter: state.counter + action.payload 
        }
        case 'DEC': return { 
            ...state, counter: state.counter + action.payload 
        }
        case 'ADDTODO': return { 
            ...state, todos: [...state.todos,  action.payload ]
        }
        default: return state;
    }
}

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload='') => ({type:'ADDTODO', payload});

/// ====

state = reducer(state, inc(1) )
state = reducer(state, inc(2) )
state = reducer(state, dec() )
state = reducer(state, addTodo('kup mleko!') )
state = reducer(state, addTodo('naucz sie Vue ;-)') )
state = reducer(state, inc(1) )